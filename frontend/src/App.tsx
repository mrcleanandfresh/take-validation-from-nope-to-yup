import React, {useState} from 'react';
import './App.css';
import {Field, Form, Formik} from "formik";

interface UserInput {
    name: string;
    age: number;
    email: string;
    website: string;
    createdOn?: Date;
}

function App() {
    // Initial user state. Notice no setter.
    const [userInput] = useState<UserInput>({
        name: '',
        age: -1,
        email: '',
        website: '',
        createdOn: undefined
    });
    const [error, setError] = useState<string | null>(null);
    const [response, setResponse] = useState(undefined);

    const handleSubmission = (values: UserInput) => {
        fetch('/', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(values)
        }).then(res => res.json())
            .then(setResponse)
            .catch(console.error)
    };

    // Simple validator
    const validate = (values: UserInput) => {
        if (!('name' in values) || values.name.length < 1) {
            throw new Error('The user must have a name.')
        }
        if (!('age' in values)) {
            throw new Error('The request must contain an age.')
        }
        if (!('email' in values) || values.email.length < 1) {
            throw new Error('The user must include an Email Address.')
        }
        if (!('website' in values) || values.website.length < 1) {
            throw new Error('The request must contain a website URL.')
        }
    };

    return (
        <div className="App">
            <Formik initialValues={userInput}
                    onSubmit={(values) => {
                        try {
                            validate(values);
                            handleSubmission(values);
                        } catch (validationError) {
                            setError(validationError.message);
                        }
                    }}>
                <Form className="mb-2">
                    {error && (
                        <div>
                            <p style={{color: "red"}}>{error}</p>
                        </div>
                    )}
                    <div className="mb-1">
                        <label htmlFor="name">Name</label>
                        <Field id="name" name="name"/>
                    </div>
                    <div className="mb-1">
                        <label htmlFor="age">Age</label>
                        <Field id="age" type="number" name="age"/>
                    </div>
                    <div className="mb-1">
                        <label htmlFor="email">Email</label>
                        <Field id="email" type="email" name="email"/>
                    </div>
                    <div className="mb-1">
                        <label htmlFor="website">Website</label>
                        <Field id="website" type="url" name="website"/>
                    </div>
                    <button type="submit">Submit</button>
                </Form>
            </Formik>
            {response && (
                <>
                    <em>Server Response:</em>
                    <pre>{JSON.stringify(response, null, 4)}</pre>
                </>
            )}
        </div>
    );
}

export default App;
