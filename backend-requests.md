# Backend Validation

## Requests
Backend validation can be hard to write out by hand, and typically is very bug-prone.

In fact, this code contains two intentional bugs in it. Handling them would cause us to create more branches of logic! At this point, we're ready for a better way.

### `POST` / Test Plan: 9 tests to success

If we want to test the validation capabilities of our API, we can test each logic branch using the following requests.

This is a lot of work to test all the different branches of logic. This is called [Cyclomatic Complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity) and if it is too high for a particular section of source code you will run into myriad problems, as well as cognitive strain for Developers.

Each logic section of the control flow in our program has a **Cyclomatic Complexity** of 3, which isn't too bad but is approaching complicated.

#### `POST` empty object
```text
$ curl -d '{}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The user must have a name."
  }
}
```

#### `POST` name is 1234
```text
$ curl -d '{"name": 1234}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The name must be a string."
  }
}
```

#### `POST` age is empty
```text
$ curl -d '{"name": "Bob Ross"}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The name must be a string."
  }
}
```

#### `POST` age is a string
Interestingly if you pass `"79b"` the `parseInt` function will parse it correctly!
```text
$ curl -d '{"name": "Bob Ross", "age": "b79"}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The request must contain an age that is a number."
  }
}
```

#### `POST` email is empty
```text
$ curl -d '{"name": "Bob Ross", "age": "79"}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The user must include an Email Address."
  }
}
```

#### `POST` email is invalid
```text
$ curl -d '{name: "Bob Ross", age: "79", "email": "bob@happytrees"}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The user must have a valid Email Address."
  }
}
```

#### `POST` website is empty
```text
$ curl -d '{"name": "Bob Ross", "age": "79", "email": "bob@happytrees.org"}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The request must contain a website URL."
  }
}
```

#### `POST` website is invalid
```text
curl -d '{"name": "Bob Ross", "age": "79", "email": "bob@happytrees.org", "website": "http//:www.pbs.org"}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get an error message back:
```json
{
  "status": "error",
  "data": null,
  "error": {
    "message": "The request must contain a valid website URL."
  }
}
```

## Successful `POST`
```text
curl -d '{"name": "Bob Ross", "age": "79", "email": "bob@happytrees.org", "website": "https://www.pbs.org"}' -H 'Content-Type: application/json' http://localhost:3030
```
You should get back the "Database" entry:
```json
{
  "status": "success",
  "data": {
    "message": "The user was successfully saved in the Database!",
    "user": {
      "id": 1619369019759,
      "name": "Bob Ross",
      "age": "79",
      "email": "bob@happytrees.org",
      "website": "https://www.pbs.org",
      "createdOn": "2021-04-25T16:43:39.759Z"
    }
  },
  "error": null
}
```