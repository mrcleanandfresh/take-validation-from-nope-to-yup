const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
dotenv.config();

const app = express();
const port = process.env.PORT ?? 3030;

function APIResponse(status, data, error = null) {
    this.status = status;
    this.data = data;
    this.error = error;
}

const URL_REGEX = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.json(new APIResponse('success', {message: 'Hello World'}))
});

app.post('/', (req, res) => {
    const {body} = req;
    if (!('name' in body)) {
        return res.json(new APIResponse('error', null, {message: 'The user must have a name.'}))
    } else {
        if (typeof body.name !== 'string') {
            return res.json(new APIResponse('error', null, {message: 'The name must be a string.'}))
        }
    }

    // Edge case: user submits -1 ...and it all breaks!
    if (!('age' in body)) {
        return res.json(new APIResponse('error', null, {message: 'The request must contain an age.'}))
    } else {
        if (isNaN(parseInt(body.age, 10))) {
            return res.json(new APIResponse('error', null, {message: 'The request must contain an age that is a number.'}))
        }
    }
    if (!('email' in body)) {
        return res.json(new APIResponse('error', null, {message: 'The user must include an Email Address.'}))
    } else {
        if (!validateEmail(body.email)) {
            return res.json(new APIResponse('error', null, {message: 'The user must have a valid Email Address.'}))
        }
    }
    if (!('website' in body)) {
        return res.json(new APIResponse('error', null, {message: 'The request must contain a website URL.'}))
    } else {
        if (!URL_REGEX.test(body.website)) {
            return res.json(new APIResponse('error', null, {message: 'The request must contain a valid website URL.'}))
        }
    }
    if (!('createdOn' in body)) {
        body.createdOn = new Date();
    }

    // Finally! Passed validation checks. Now proceed to saving in the Database.
    return res.json(new APIResponse('success', {
        message: 'The user was successfully saved in the Database!',
        user: {id: Date.now(), ...body}
    }));
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});