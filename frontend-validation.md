# Frontend Validation

## Initial State
In our app we've set the initial state to:
```typescript
// Initial user state. Notice no setter.
const [userInput] = useState<UserInput>({
    name: '',
    age: -1,
    email: '',
    website: '',
    createdOn: undefined
});
```

This is because the Backend expects an Object with the following shape for the body of the request:

- `name` - (Required) Must be a string.
- `age` - (Required) Must be a number.
- `email` - (Required) Must be a valid Email.
- `website` - (Required) Must be a valid URL.
- `createdOn` - Must be a Date.

However, you may notice that technically `name`, `email` and `website` are empty strings. So unless we handle those as `null` values, then they pass the "Required" test.

Certainly `-1` is a valid number, but is not a valid age!

So we may have some problems with this initial state that we've got to work-around already...

### Test Plan

1. Click submit without any information, there should be an error message indicating the first field is required.
2. Click submit after inputting a valid `name`, there should be an error indicating `email` is required! (Wait, what about `age`?)
3. Click submit after inputting a valid `email`, there should be an error indicating `website` is required.
4. Click submit after inputting a valid `website`, it should submit successfully. (But should it have been successful?)